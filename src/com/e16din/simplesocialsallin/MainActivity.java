package com.e16din.simplesocialsallin;

import com.e16din.simplesocials.core.interfaces.BaseSocialManger;
import com.e16din.simplesocials.core.interfaces.SuccessListener;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends BaseAllSocialsActivity {

	private static final String TAG_DEBUG = "debug";

	private SuccessListener listener = new SuccessListener() {
		@Override
		public void onSuccess(BaseSocialManger manager, String supportStr) {
			Toast.makeText(MainActivity.this, "onSuccess", Toast.LENGTH_SHORT).show();
			Log.d(TAG_DEBUG, "onSuccess: " + manager.getName() + " + " + supportStr);
		}

		@Override
		public void onFail(String error, BaseSocialManger manager) {
			Toast.makeText(MainActivity.this, "onFail", Toast.LENGTH_SHORT).show();
			Log.d(TAG_DEBUG, "onFail: " + manager.getName() + " + " + error);
		}

		@Override
		public void onCancel() {
			Toast.makeText(MainActivity.this, "onCancel", Toast.LENGTH_SHORT).show();
			Log.d(TAG_DEBUG, "onCancel");
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void onBtnFacebookClick(View v) {
		if (!getFbManager().isAuth())
			getFbManager().login(listener);
		else
			getFbManager().logout(listener);
	}

	public void onBtnVkontakteClick(View v) {
		if (!getVkManager().isAuth())
			getVkManager().login(listener);
		else
			getVkManager().logout(listener);
	}

	public void onBtnTwitterClick(View v) {
		if (!getTwManager().isAuth())
			getTwManager().login(listener);
		else
			getTwManager().logout(listener);
	}
}
