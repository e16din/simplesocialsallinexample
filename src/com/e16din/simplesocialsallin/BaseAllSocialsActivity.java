package com.e16din.simplesocialsallin;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.widget.Toast;

import com.e16din.simplesocials.core.BaseSocialActionBarActivity;
import com.e16din.simplesocials.core.interfaces.BaseSocialManger;
import com.e16din.simplesocials.managers.FacebookManager;
import com.e16din.simplesocials.managers.TwitterManager;
import com.e16din.simplesocials.managers.VkontakteManager;

public class BaseAllSocialsActivity extends BaseSocialActionBarActivity {

	private static VkontakteManager vkManager = null;
	private static FacebookManager fbManager = null;
	private static TwitterManager twManager = null;

	private static Dialog pdLoading = null;

	@Override
	public BaseSocialManger initSocialNetworks() {
		this.setVkManager(new VkontakteManager(this, "*"));//TODO: set your vk app id
		
		this.setFbManager(new FacebookManager(this, "*"));//TODO: set your fb app id
		
		this.setTwManager(new TwitterManager(this));
		twManager.setConsumerKey("*");//TODO: set your twitter consumer key 
		twManager.setConsumerSecret("*");//TODO: set your twitter consumer secret

		return this.getVkManager();
	}

	protected void showPostedNotice() {
		Toast.makeText(this, getString(R.string.posted), Toast.LENGTH_SHORT).show();
	}

	public void showProgress() {
		if (pdLoading != null && pdLoading.isShowing())
			return;

		pdLoading = ProgressDialog.show(this, null, getString(R.string.sending));
	}

	public void hideProgress() {
		if (pdLoading != null) {
			pdLoading.dismiss();
			pdLoading = null;
		}
	}

	public VkontakteManager getVkManager() {
		return vkManager;
	}

	public void setVkManager(VkontakteManager vkManager) {
		this.vkManager = vkManager;
	}

	public FacebookManager getFbManager() {
		return fbManager;
	}

	public void setFbManager(FacebookManager fbManager) {
		this.fbManager = fbManager;
	}

	public TwitterManager getTwManager() {
		return twManager;
	}

	public void setTwManager(TwitterManager twManager) {
		this.twManager = twManager;
	}
}
